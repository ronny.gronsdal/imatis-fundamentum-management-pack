# Imatis Fundamentum Management Pack

## Description
Imatis Fundamentum Management pack for SCOM er en pakke som er ment til å la deg enkelt overvåke din installasjon av Imatis Fundamentum. 
Pakken er utviklet i Helse Vest av Helse Vest IKT sitt driftssenter for å imøtekomme overvåkingsbehovene rundt innføringen av Imatis Fundamentum i sykehusene, da spesielt Haukeland og Haraldsplass i Bergen.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Denne pakken bør seales i MP Author eller lignende verktøy før man installerer den, dette for å sikre at man ikke legger inn overrides direkte i MP og at klassene kan brukes i egne Managementpakker.

Utover dette er det bare å installere den med importfunksjonen i konsollet til SCOM.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Forfatter av denne management pakken er Ronny Grønsdal.

## License
Pakken er open source og lisensiert under GPL2 som du finner i repoet i filen LICENCE

## Project status
Foreløpig er det kun discovery av servere og definisjon av ulike klasser som er inkludert i pakken. 
Det er også laget en mappe som dukker opp under Monitoring med stateview for hver av servertypene Imatis Servere, Imatis VisiSurvey Servere og Imatis Log Servere.
